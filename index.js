const {
	collectible,
	discord_token,
	discord_channel
} = require('./config.json');
const fetch = require("node-fetch");
const Discord = require('discord.js');

function getting() {
	if (collectible == "true") {
		fetch(`https://api.hearthstonejson.com/v1/39282/jaJP/cards.collectible.json`)
		.then(response => response.json())
		.then(data => {
			public_data = data
		})
		fetch(`https://api.hearthstonejson.com/v1/39282/frFR/cards.collectible.json`)
		.then(response => response.json())
		.then(data => {
			french_data = data
		})
	} else {
		fetch(`https://api.hearthstonejson.com/v1/39282/jaJP/cards.json`)
		.then(response => response.json())
		.then(data => {
			public_data = data
		})
		fetch(`https://api.hearthstonejson.com/v1/39282/frFR/cards.json`)
		.then(response => response.json())
		.then(data => {
			french_data = data
		})
	}
	setTimeout(choosing, 1400)
}

function choosing() {
	if (public_data !== "Placeholder") {
		var compter = -1
		for (obj in public_data) {
			compter += 1
		}
		nombre = Math.floor((Math.random() * compter) + 0)
		identifiant = public_data[nombre].id
		nom = public_data[nombre].name
		try {
			texte = public_data[nombre].text
			texte = texte.split('[x]').join('')
			texte = texte.split('<b>').join('')
			texte = texte.split('</b>').join('')
			texte = texte.split('<i>').join('')
			texte = texte.split('</i>').join('')
			texte = texte.split('$').join('')
		}
		catch {
			console.log("La carte n'a pas de texte ! (JP)")
		}
		if (discord_ready == "true") {
			try {
				client.channels.get(discord_channel).send({files: [`https://art.hearthstonejson.com/v1/render/latest/jaJP/512x/${identifiant}.png`]})
				.catch(error => {
					console.error(error)
					console.log("Incapable d'afficher l'artwork de la carte. (JP)")
				})
				client.channels.get(discord_channel).send("Nom: " + nom)
				try {
					client.channels.get(discord_channel).send("Texte: " + texte)
				} catch {
					console.log("Le texte n'a pas pu être envoyé ! (JP)")
				}
			}
			catch {
				console.log("Mauvais salon Discord, changez discord_channel dans config.json")
				process.exit()
			}
		} else {
			console.log("Il semblerait que le bot n'ait pas réussit à se connecter à Discord, vérifiez discord_token dans config.json")
			console.log("Le bot va retenter de se connecter et prendre une nouvelle carte dans 10 secondes...")
			setTimeout(discord_login, 10000)
			setTimeout(choosing, 12000)
		}
	} else {
		console.log("Erreur, les données desirées n'ont pas pu être obtenues")
		console.log("Nouvel essai dans 5 secondes...")
		setTimeout(getting, 5000)
	}
}

function translate() {
	try {
		try {
			nom_fr = french_data[nombre].name
			texte_fr = french_data[nombre].text
			texte_fr = texte_fr.split('[x]').join('')
			texte_fr = texte_fr.split('<b>').join('')
			texte_fr = texte_fr.split('</b>').join('')
			texte_fr = texte_fr.split('<i>').join('')
			texte_fr = texte_fr.split('</i>').join('')
			texte_fr = texte_fr.split('$').join('')
			texte_fr = texte_fr.split('|4(point,points)').join('point ou points')
		}
		catch {
			console.log("La carte n'a pas de texte ! (FR)")
		}
		client.channels.get(discord_channel).send({files: [`https://art.hearthstonejson.com/v1/render/latest/frFR/512x/${identifiant}.png`]})
		.catch(error => {
			console.error(error)
			console.log("Incapable d'afficher l'artwork de la carte. (FR)")
		})
		client.channels.get(discord_channel).send("Nom: " + nom_fr)
		try {
			client.channels.get(discord_channel).send("Texte: " + texte_fr)
		}
		catch {
			console.log("Le texte n'a pas pu être envoyé ! (FR)")
		}
	}
	catch {
		console.log("Vous devez faire newcard avant translate")
	}
}

function discord_login() {
	client = new Discord.Client()
	client.login(discord_token)
	.catch(console.error)
	client.once('ready', () => {
		discord_ready = "true"
		console.log("\nConnecté à Discord !\n")
	});
	client.on('error', error => {
		console.error('(Hearthstone JtF, Discord) The websocket connection encountered an error:', error);
	});
	client.on('message', msg => {
		if (msg.content === 'newcard') {
			getting()
		}
		if (msg.content === 'translate') {
			translate()
		}
	});
}

function discord_logoff() {
	process.exit()
}

discord_ready = "false"
public_data = "Placeholder"
discord_login()
