C'est un outil que j'ai développé pour mon intérêt.

Avec la commande "newcard", vous affichez sur votre serveur Discord une carte Hearthstone dans la langue japonaise.

Le nom et le texte de celle-ci est aussi affiché au dessus de la carte au cas-où vous voulez rechercher sur Internet certains charactères, et au cas-où le texte écrit sur
la carte est mal lisible.

Avec la commande "translate", vous affichez sur votre serveur Discord la même carte Hearthstone, mais dans la langue française.

Ainsi, vous pouvez comparer les différents éléments de la carte entre les deux versions et potentiellement apprendre le japonais.

Essayez de remarquer des éléments communs entre deux cartes en japonais puis voyez quel est cet élément en commun en français, par example !

Le code de ce programme est basé sur https://gitlab.com/Isterix/hearthstone-rcg